<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php
    $department=array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
    ?>
    <form class="form-search" method="POST" enctype="multipart/form-data" action="<?php 
         echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
            <div class="department__input">
                <label>Phân khoa</label>
                
                <select name="department" class="custom__width" id="department">
                    <option value=""></option>
                    <?php
                    foreach (array_keys($department) as $dep) {
                        echo '
                                <option value="'.$department[$dep].'">' . $department[$dep] . '</option>
                            ';
                    }
                    ?>
                </select>
            </div>
            <div class="name__input">
                <label>Từ khóa</label>
                <input class="input_name" type="text" name="name" value="" id="tukhoa" onchange="changeInputName()">
            </div>
        <div style="display: flex;">
            <button class="dki search" id="search">Tìm kiếm</button>
            <button type="button" onclick="deleteAll()" class="dki delete" id="delete">Xóa</button>
        </div>
        <p class="num">Số sinh viên tìm thấy: XXX</p>
        <a href="./dangki.php"><input class="dki add" type="button" value="Thêm">
        </a>
        <table>
            <tr>
                <th class="col1"><p>No</p></th>
                <th class="col2"><p>Tên sinh viên</p></th>
                <th class="col3"><p>Khoa</p></th>
                <th class="col4"><p>Action</p></th>
            </tr>
            <tr>
                <td class="col1"><p>1</p></td>
                <td class="col2"><p>Nguyễn Văn A</p></td>
                <td class="col3"><p>Khoa học máy tính</p></td>
                <td class="col4">
                    <button class="table-btn">Xoá</button>
                    <button class="table-btn">Sửa</button>
                </td>
            </tr>
            <tr>
                <td class="col1"><p>2</p></td>
                <td class="col2"><p>Nguyen Van B</p></td>
                <td class="col3"><p>Khoa học máy tính</p></td>
                <td class="col4"><button class="table-btn">Xoá</button><button class="table-btn">Sửa</button></td>
            </tr>
            <tr>
                <td class="col1"><p>3</p></td>
                <td class="col2"><p>Nguyễn van C</p></td>
                <td class="col2"><p>Khoa học vật liệu</p></td>
                <td class="col4"><button class="table-btn">Xoá</button><button class="table-btn">Sửa</button></td>
            </tr>
            <tr>
                <td class="col1"><p>4</p></td>
                <td class="col2"><p>Nguyen Van E</p></td>
                <td class="col3"><p>Khoa học vật liệu</p></td>
                <td class="col4"><button class="table-btn">Xoá</button><button class="table-btn">Sửa</button></td>
            </tr>
        </table>
    </form>
</body>
</html>

<script src="index.js"></script>